import { serverHttp } from "./app";

const PORT = 4000;
// windowsKey + . => para mostrar os emojis
serverHttp.listen(PORT, () =>
  console.log(`🚀 Server is running on PORT: ${PORT}`)
);
